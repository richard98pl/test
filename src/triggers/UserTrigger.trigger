trigger UserTrigger on User (after insert) {
    //0PS5I000000MmmoWAC community permission set
    //partner community profile 00e5I000000IezsQAC
    for(User u : Trigger.New){
        if(u.ProfileId == '00e5I000000IezsQAC'){
            CommunityUserTriggerHandler.permissionSetAssigment(u.Id);
        }
    }
}