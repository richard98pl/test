trigger OrderTrigger on Order (after insert, after update) {
    List<Order> doneOrders = new List<Order>();
    for(Order o : Trigger.New){
        if(o.Status == 'Done'){
            doneOrders.add(o);
        }
    }
    
    List<OrderItem> orderItemsWithStatusDone = [SELECT Product2Id, Quantity from OrderItem where OrderId in :doneOrders];
    if(orderItemsWithStatusDone != null){
        Map<Id,Decimal> productsAndQuantity = new Map <Id,Decimal>();
       
        Set<Id> productsIds = new Set<Id>();
        for(OrderItem oi : orderItemsWithStatusDone){
            productsIds.add(oi.Product2Id);
            if(productsAndQuantity.get(oi.Product2Id) == null){
                productsAndQuantity.put(oi.Product2Id,oi.Quantity);
            }else{
                productsAndQuantity.put(oi.Product2Id,productsAndQuantity.get(oi.Product2Id) + oi.Quantity);
            }
        }
        
        List<Product2> products = [SELECT Id,Amount__c from Product2 where Id in : productsIds];
        for(Product2 product : products){
            product.Amount__c -= productsAndQuantity.get(product.Id);
        }
        upsert products;
    }
    
}