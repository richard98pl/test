trigger ProductTrigger on Product2 (after insert) {
    List<PricebookEntry> pbe = new List<PricebookEntry>();
    for(Product2 product : Trigger.new){
        PricebookEntry standardtmppbe = new PricebookEntry();
        standardtmppbe.IsActive = true;
        standardtmppbe.Pricebook2Id = '01s5I00000096O6QAI';
        standardtmppbe.Product2Id = product.Id;
        standardtmppbe.UnitPrice = 0;
        
        PricebookEntry tmppbe = new PricebookEntry();
        tmppbe.IsActive = true;
        tmppbe.Pricebook2Id = '01s5I00000096O5QAI';
        tmppbe.Product2Id = product.Id;
        tmppbe.UnitPrice = 0;
        
        pbe.add(standardtmppbe);
        pbe.add(tmppbe);        
    }
    INSERT pbe;
}