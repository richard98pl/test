trigger QuoteTrigger on Quote (after update) {
        if(Trigger.isUpdate){
            if(QuoteHandler.escapingRecursionBoolean){
                QuoteHandler.SetQuoteDocumentsCheckboxesOnQoute(trigger.new);
                System.debug('Quote is updating!');
            }
        } 
}