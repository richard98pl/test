public class QuoteHandler {
    public static Boolean escapingRecursionBoolean = true;
    public static Integer triggerNumber = 0;
    
    public static void SetQuoteDocumentsCheckboxesOnQoute(List<Quote> triggerQuotes) {

        QuoteHandler.escapingRecursionBoolean = false;

        Set<String> quoteFields = Schema.SObjectType.Quote.fields.getMap().keySet();
        Map<String, String> quoteCustomMetadataRecordsMap = getMapOfQuoteCustomMetadataFields(quoteFields);

        List<Quote> quotes = QuoteHandler.queryTheSameQuotes(triggerQuotes, quoteCustomMetadataRecordsMap, quoteFields);
        Map<Id, List<QuoteLineItem>> mapOfQuoteAndLineItems = QuoteHandler.mapQuotesAndItsQuoteLines(quotes);

        for (Quote quote : quotes) {
            uncheckAllCheckboxes(quote, quoteCustomMetadataRecordsMap, quoteFields);
            checkPresentUCCheckboxes(quote, mapOfQuoteAndLineItems, quoteCustomMetadataRecordsMap, quoteFields);
        }

        update quotes;
        QuoteHandler.escapingRecursionBoolean = true;
    }

    public static Map<Id, List<QuoteLineItem>> mapQuotesAndItsQuoteLines(List<Quote> quotes) {
        Map<Id, List<QuoteLineItem>> mapQuoteAndLineItems = new Map<Id, List<QuoteLineItem>>();
        List<QuoteLineItem> allQuoteLinesOfQuotes = [
                SELECT ID,Product2Id,HF2_PPUC_Identifier__c,QuoteId
                FROM QuoteLineItem
                WHERE QuoteId IN :quotes
        ];
        
        for (Quote p : quotes) {
            mapQuoteAndLineItems.put(p.Id, new List<QuoteLineItem>());
            for (QuoteLineItem quoteLine : allQuoteLinesOfQuotes) {
                if (quoteLine.QuoteId == p.Id) {
                    if (quoteLine != null)mapQuoteAndLineItems.get(p.Id).add(quoteLine);
                }
            }
        }
        return mapQuoteAndLineItems;
    }

    public static Map<String, String> getMapOfQuoteCustomMetadataFields(Set<String> quoteFields) {
        Map<String, String> resultMap = new Map<String, String>();
        List<HF2_PPUC_Identifier_Quote__mdt> quoteCustomMetadataRecords = [SELECT HF2_PPUC_Identifier__c, HF2_Referral_Field__c FROM HF2_PPUC_Identifier_Quote__mdt];

        Map<String, List<HF2_PPUC_Identifier_Quote__mdt>> alreadySetMetdataRecordsByResponsibleEmail = new Map<String, List<HF2_PPUC_Identifier_Quote__mdt>>();
        Map<String, List<HF2_PPUC_Identifier_Quote__mdt>> missingAPIMetdataRecordsByResponsibleEmail = new Map<String, List<HF2_PPUC_Identifier_Quote__mdt>>();
        String unacessableQuoteFields = '';
        
        for (HF2_PPUC_Identifier_Quote__mdt metadataRecord : quoteCustomMetadataRecords) {
            if (quoteFields.contains(metadataRecord.HF2_Referral_Field__c.toLowerCase())) {
                if (!resultMap.containsKey(metadataRecord.HF2_PPUC_Identifier__c)) {
                    resultMap.put(metadataRecord.HF2_PPUC_Identifier__c, metadataRecord.HF2_Referral_Field__c);
                    if(!QuoteHandler.isFieldAccessibleForQuote(metadataRecord.HF2_Referral_Field__c))unacessableQuoteFields += metadataRecord.HF2_Referral_Field__c + ', ';
                } else {
                    //already has checkbox for this PPUC exception
                }
            } else {
                //no such API on Quote exception
            }
        }
        //System.debug('The uneacessable quote Fields --> ' + unacessableQuoteFields);   
        return resultMap;
    }

    public static List<Quote> queryTheSameQuotes(List<Quote> triggerQuotes,
        Map<String, String> quoteCustomMetadataRecordsMap, Set<String> quoteFields) {
        String updatedQuoteQuery = 'SELECT ID,';
        Set<String> checkWhatAreWeQueryingSetAvoidingDuplicates = new Set<String>();
        for (String metaDataCheckboxAPI : quoteCustomMetadataRecordsMap.values()) {
            if (quoteFields.contains(metaDataCheckboxAPI.toLowerCase())) {
                if(QuoteHandler.isFieldAccessibleForQuote(metaDataCheckboxAPI) ){
                    if(!checkWhatAreWeQueryingSetAvoidingDuplicates.contains(metaDataCheckboxApi)){
                        updatedQuoteQuery += ' ' + metaDataCheckboxApi + ',';
                        checkWhatAreWeQueryingSetAvoidingDuplicates.add(metaDataCheckboxApi);
                    }
                }else{
                    
                }                
            }
        }
        
        Set<Id> quotesIds = (new Map<Id, Quote>(triggerQuotes)).keySet();
        updatedQuoteQuery += ' Name from Quote WHERE ID IN ' + QuoteHandler.inClausify(quotesIds);
        List<Quote> requeriedQuotes = database.query(updatedQuoteQuery);
        return requeriedQuotes;
    }

    public static void uncheckAllCheckboxes(Quote quote, Map<String, String> quoteCustomMetadataRecordsMap, Set<String> quoteFields) {
        for (String metaDataCheckboxAPI : quoteCustomMetadataRecordsMap.values()) { //first uncheck all
            if (quoteFields.contains(metaDataCheckboxAPI.toLowerCase()) && QuoteHandler.isFieldAccessibleForQuote(metaDataCheckboxAPI) ) {
                quote.put(metaDataCheckboxAPI, false);
            }
        }
    }

    public static void checkPresentUCCheckboxes(Quote quote, Map<Id, List<QuoteLineItem>> mapOfQuoteAndLineItems,
            Map<String, String> quoteCustomMetadataRecordsMap, Set<String> quoteFields) {
        List<QuoteLineItem> tmpQuoteLines = mapOfQuoteAndLineItems.get(quote.Id);
        for (QuoteLineItem quoteLine : tmpQuoteLines) { //check again those that are in Quote
            if (quoteCustomMetadataRecordsMap.containsKey(quoteLine.HF2_PPUC_Identifier__c) &&
                    quoteFields.contains(quoteCustomMetadataRecordsMap.get(quoteLine.HF2_PPUC_Identifier__c).toLowerCase()) &&
                      QuoteHandler.isFieldAccessibleForQuote(quoteCustomMetadataRecordsMap.get(quoteLine.HF2_PPUC_Identifier__c))) {
                quote.put(quoteCustomMetadataRecordsMap.get(quoteLine.HF2_PPUC_Identifier__c), true);
            }
        }
    }

    public static String inClausify(Set<Id> ids) {
        String inClause = String.format('(\'\'{0}\'\')',
                new List<String>{
                        String.join(new List<Id>(ids), '\',\'')
                });
        return inClause;
    }
    
    public static SObjectType schemaTypeQuote = Schema.getGlobalDescribe().get('Quote');
    public static Map<String, SObjectField> fields = schemaTypeQuote.getDescribe().fields.getMap();
    
    public static Boolean isFieldAccessibleForQuote(String fieldName){     
        DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
        return fieldDescribe.isAccessible() && fieldDescribe.isUpdateable();
    }
    
}